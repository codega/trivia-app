# trivia-app

## Project-development comments
When we started the project, we had not yet learnt how to use the vue-router.
We did nearly the entire project without the router and found a solution in using v-if on the components.
If we did the project again from scratch, with what we know now - we would have used the router.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
